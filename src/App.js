import './App.css';
import {Root} from "./dz-7/Root";

function App() {
  return (
    <div className="App">
        <Root />
    </div>
  );
}

export default App;
